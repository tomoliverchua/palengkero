// SHOW categories on table brand in action2.php
$(document).ready(function(){
	cat();
	brand();
	product();
	function cat(){
		$.ajax({
			url  :"action2.php",
			method: "POST",
			data : {category:1}, // name of variable passes to action2.php
			success : function(data){
				$("#get_category").html(data);
			}
		})
	}
	
	// SHOW brand on table brand in action2.php
	function brand(){
		$.ajax({
			url  :"action2.php",
			method: "POST",
			data : {brand:1}, // name of variable passes to action2.php
			success : function(data){
				$("#get_brands").html(data);
			}
		})
	}
	
	// SHOW PRODUCTS ON index.php
	function product(){
		$.ajax({
			url  :"action2.php",
			method: "POST",
			data : {getProduct:1}, // name of variable passes to action2.php
			success : function(data){
				$("#get_product").html(data);
			}
		})
	}
	
	// for category clicks
	$("body").delegate(".category","click",function(event){
			event.preventDefault();
			var cid = $(this).attr('cid');
			
					$.ajax({
					url  	:	"action2.php",
					method	: "POST",
					data 	: {get_selected_Category:1,cat_id:cid}, // name of variable passes to action2.php
					success : function(data){
						$("#get_product").html(data);
					}
				})
			
		})
	
	// for Brand clicks
	$("body").delegate(".selectBrand","click",function(event){
			event.preventDefault();
			var bid = $(this).attr('bid');
			
					$.ajax({
					url  	:	"action2.php",
					method	: "POST",
					data 	: {selectBrand:1,brand_id:bid}, // name of variable passes to action2.php
					success : function(data){
						$("#get_product").html(data);
					}
				})
			
		})
	
	
	// FOR SEARCH FUNCTION
	$("#search_btn").click(function(){
		var keyword = $("#search").val(); // from textbox
		if (keyword != ""){
			
			$.ajax({
					url  	:	"action2.php",
					method	: "POST",
					data 	: {search:1,keyword:keyword}, // name of variable passes to action2.php
					success : function(data){
						$("#get_product").html(data);
					}
				})
		}
		return false;
	})
	
	//FOR Registration pop UP
//	$("#signup_button").click(function(event){
	$("body").delegate("#signup_button","click",function(event){
		event.preventDefault();
		
		$.ajax({
			url 	: "registration.php",
			method 	: "POST",
			data 	: $("form").serialize(),
			success : function(data){
				$("#signup_msg").html(data);
			}
		});
	});
	
	// FOR LOGIN
	$("#login").click(function(event){
		event.preventDefault();
		var email = $("#email").val();
		var pass = $("#password").val();
		$.ajax({
			url : "login.php",
			method : "POST",
			data  :  {userLogin:1,userEmail:email,userPassword:pass},
			success : function(data){

				if(data == "Login"){			
				  	$("#log_success").modal();
					$("#myModal_log").hide();
				 	window.location.href ="profile.php";
				}
				if(data == "Wrong"){
					$("#log_fail").modal();
	
				}
			}
		});
	});
	
	
	
	$("#btn_register").click(function(event){
		event.preventDefault();
		
		window.location.href ="cust_registration.php";
					
	});
	
	
	// For add to cart!
	 cart_count(); // repeat the method!
	$("body").delegate("#product","click",function(event){
		event.preventDefault();
		var p_id = $(this).attr('pid');
		$.ajax({
			url : "action2.php",
			method : "POST",
			data : {addToProduct:1,prodId:p_id},
			success : function(data){
			 $("#product_msg").html(data);
			//alert(data); // For pop up ng added cart or hindi login....!
		     cart_count(); // para magrefresh yung count!
		}
		})
	})
	
	// Cart notification!
	cart_container();
	function cart_container(){
		$.ajax({
			url : "action2.php",
			method : "POST",
			data : {get_cart_product:1},
			success : function(data){
				$("#cart_product").html(data);
		}
		})
		
	}
	
	// cart Count!!
	function cart_count(){
		$.ajax({
			url : "action2.php",
			method : "POST",
			data : {cart_count:1},
			success : function(data){
				$(".badge").html(data);
		}
		})
	}
	
	// FOR cart_container. shows on notification!
	$("#cart_container").click(function(event){
		event.preventDefault();
		$.ajax({
			url : "action2.php",
			method : "POST",
			data : {get_cart_product:1},
			success : function(data){
			 $("#cart_product").html(data);
		}
		})
	})
					  
	
	//For Cart checkout
	cart_checkout();
	function cart_checkout(){
		$.ajax({
			url : "action2.php",
			method : "POST",
			data : {cart_checkout:1},
			success : function(data){
				$("#cart_checkout").html(data);
			}
		})
	}
	
	// automatic change Price episode 24
	$("body").delegate(".qty","keyup",function(){
		var pid = $(this).attr("pid"); // pid instance for product_id!
//		alert(bid); for p_id;
		var qty = $("#qty-"+pid).val(); // get the value of qty from action2.php
		var price = $("#price-"+pid).val(); // get the value of price from action.php
		var total = qty * price;  // COMPUTATION OF TOTAL PRICE!
//		alert(total); // messsagebox total output
		$("#total-"+pid).val(total) // var total passes to #total-pid on action2.php
	})
	
	// To REMOVE products from cart.php! 
	$("body").delegate(".remove","click",function(event){
		event.preventDefault();
		var pid = $(this).attr("remove_id");
		$.ajax({
			url : "action2.php",
			method : "POST",
			data : {removeFromCart:1,removeId:pid}, // passing to action2.php remove!
			success : function(data){
//				alert(data); // laman ng echo sa php! TAKE NOTE DAPAT 1 LINER LANG NG OUTPUT
				$("#cart_msg").html(data);
				cart_checkout();
			}
		})
		
	})
	
	// To UPDATE products from cart.php! 
	$("body").delegate(".update","click",function(event){
		event.preventDefault();
		//		alert(pid); foR showing product ID
		var pid = $(this).attr("update_id");
		var qty = $("#qty-"+pid).val();
		var price = $("#price-"+pid).val();
		var total = $("#total-"+pid).val();
		$.ajax({
			url : "action2.php",
			method : "POST",
			data : {updateProduct:1,updateId:pid,qty:qty,price:price,total:total},
			success : function(data){
				$("#cart_msg").html(data); 
				cart_checkout();
			}
		})
	})
	
	page(); // Page function for pagination...!
	function page(){
		$.ajax({
			url : "action2.php",
			method : "POST",
			data : {page:1},
			success : function(data){
				$("#pageno").html(data);
			}
		})
	}
	
	// next page for list of products!
	$("body").delegate("#page","click",function(){
		var pn = $(this).attr("page");
		$.ajax({
			url : "action2.php",
			method : "POST",
			data : {getProduct:1,setPage:1,pageNumber:pn},
			success : function(data){
//				alert(data);
				$("#get_product").html(data);
			}
		})
	})
	
	// For login validation
	$("body").delegate(".login_val","click",function(){
		event.preventDefault();
		$.ajax({
			url : "action2.php",
			method : "POST",
			data : {login_validate:1},
			success : function(data){
				if(data){
					alert(data);
				window.location.assign("relogin.php");
				}
			}
		});
	});
	
	// for account settings
	showAccount();
	function showAccount(){
		$.ajax({
			url : "action2.php",
			method : "POST",
			data : {show_Account:1},
			success : function(data){
				$("#show_account").html(data);	
		}
		});
	}
	
	
	$("body").delegate("#change_account","click",function(){
		event.preventDefault();
		
	});
	
	
	
	$("body").delegate("#btn_view","click",function(){
		$("#view_items").modal();
		
	});
	
	$("body").delegate("#change_confirm","click",function(){
		var fn = $("#f_name").val();
		var ln = $("#l_name").val();
		var email = $("#email").val();
		var mobile = $("#mobile").val();
		var brgy = $("#brgy").val();
		var city = $("#city").val();
		var town = $("#province").val();
		var pass = $("#password").val();
		var userid = $("#user_id").val();
		$.ajax({
			url : "action2.php",
			method : "POST",
			data : {Update_Account:1,f_name:fn,l_name:ln,email:email,mobile:mobile,brgy:brgy,city:city,town:town,userid:userid,password:pass},
			success : function(data){
				showAccount();
				if(data){
					$("#update_success").modal();
//					$("#Change_Account").hide();
				}
				
		}
		});
		
	});
	
	
	
	// Latest Products
	showLatest();
	function showLatest(){
		$.ajax({
			url : "action2.php",
			method : "POST",
			data : {Latest_Products:1},
			success : function(data){
				$("#latest_products").html(data);			
		}
		});
	}
	
	$("body").delegate("#datecollect","click",function(){
		
		alert("click");
	});
//	Page_show_all(); // Page function for pagination...!
//	function Page_show_all(){
//		$.ajax({
//			url : "action2.php",
//			method : "POST",
//			data : {Page_Show_All:1},
//			success : function(data){
////				$("#show_all").html(data);
//				alert(data);
//			}
//		})
//	}
})