$(document).ready(function(){
	//shows data from database!se 
	// this shows data from database products!
	dataTables();
	function dataTables(){
		$.ajax({
			url : "experiment.php",
			method : "POST",
			data : {data_table:1},
			success : function(data){
				$("#show").html(data);
//				alert(data);
			}
		});
	}
	
	showArchive();
	function showArchive(){
		$.ajax({
			url : "experiment.php",
			method : "POST",
			data : {show_Archive:1},
			success : function(data){
				
				$("#show-archive").html(data);
		}
		});
	}
	
	

	// EDIT for Product maintenance.php!
	$("body").delegate(".btn-edit","click",function(){
		event.preventDefault();
		
		var pid = $(this).attr('pid');// get the value of pid
		$.ajax({
			url : "experiment.php",
			method : "POST",
			data : {get_select_data:1,search_id:pid},
			success : function(data){
				$("#inputs").html(data);
//				alert(pid);
			}
		});
	});
	

	
	// Trigger to Close edit area!
	$("body").delegate(".btn-close","click",function(){
		event.preventDefault();
		$("#inputs").hide();
	});
	
	// Trigger to 
	$("body").delegate(".btn-edit","click",function(){
		event.preventDefault();
		
		$("#inputs").show();
	});
	
	// Delete for Product Maitenance
	$("body").delegate(".btn-delete","click",function(){
		event.preventDefault();
		
		var del = $(this).attr('did');
		
		$.ajax({
			url : "experiment.php",
			method : "POST",
			data : {delete_Data:1,del_id:del},
			success : function(data){
				if(confirm("Are you sure to delete")){ // ok cancel
					
				dataTables();
				$("#inputs").html(data);
					
				}
			}
		});
	});
	
	// for Product archive Buttons!
	$("body").delegate(".btn-delete-archive","click",function(){
		event.preventDefault();
		
		var archive_id = $(this).attr('archive_id');
		
		$.ajax({
			url : "experiment.php",
			method : "POST",
			data : {Archive_restore:1,archive_id:archive_id},
			success : function(data){
				if(confirm("Are you sure to restore this data?")){
					showArchive();
					$("#show-archive").html(data);
					if(data){
						alert("Restore!");
					}
				}
			}
		})
		
	})
	
	// For Pagination of Product Maintenance
	page();
	function page(){
		$.ajax({
			url : "experiment.php",
			method : "POST",
			data : {page:1},
			success :function(data){
				$("#pageno").html(data);
		}
		})
	}
	//for Pagintion of Product Maintenance
	$("body").delegate("#page","click",function(){
		var pn = $(this).attr("page");
		$.ajax({
			url : "experiment.php",
			method : "POST",
			data : {data_table:1,setPage:1,pageNumber:pn},
			success :function(data){
				$("#show").html(data); // show tables;!!!
		}
		})
	});
	
	
	// ADMIN SIDE TO!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	
	
	// show category table from maintenance
	editCat_Brand();
	function editCat_Brand(){
		$.ajax({
			url : "experiment.php",
			method : "POST",
			data : {editCat_Brand:1},
			success : function(data){
				
				$("#category_brand").html(data);
		}
		});
	}
	
	
	// Add Category table
	$("#btn-add").click(function(){
		var category = $("#category").val(); // from textbox
		$.ajax({
			url : "experiment.php",
			method : "POST",
			data : {add_Category:1,Cat_name:category},
			success : function(data){
			editCat_Brand();
				if(data){
					alert(data);
				}
				
			}
		});
		return false;
	});
	
	// Edit Categories table
	$("body").delegate(".btn-edit-category","click",function(){
		event.preventDefault();
		
		var cat_id = $(this).attr('cid');
		
		$.ajax({
			url : "experiment.php",
			method : "POST",
			data : {Category_Edit:1,cat_id:cat_id},
			success : function(data){
				$("#show-category-edit").html(data);
			}
		})
	});
	
	//for delete
	$("body").delegate(".btn-delete-category","click",function(){
		event.preventDefault();
		
		var cat_id = $(this).attr('cid');
		
		$.ajax({
			url : "experiment.php",
			method : "POST",
			data : {Category_Delete:1,cat_id:cat_id},
			success : function(data){
				if(confirm("Are you sure to restore this data?")){
					editCat_Brand();
					alert(data);
				}
			}
		})
		
	});
	
	// for category close
 	$("body").delegate(".btn-close-category","click",function(){
		event.preventDefault();
		$("#show-category-edit").hide();
	});
	
	// for category edit
	$("body").delegate(".btn-edit-category","click",function(){
		event.preventDefault();
		$("#show-category-edit").show();
	});
	
	//FOR BRAND SELECTION!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	
	// show brand table
	Brand_table();
	function Brand_table(){
		$.ajax({
			url : "experiment.php",
			method : "POST",
			data : {brand_table:1},
			success : function(data){
				
				$("#brand_selection").html(data);
		}
		});
	}
	
	// Add Category table
	$("#btn-add-brand").click(function(){
		var brand = $("#brand").val(); // from textbox
		$.ajax({
			url : "experiment.php",
			method : "POST",
			data : {add_Brand:1,Brand_name:brand},
			success : function(data){
			
				Brand_table();
				if(data){
					alert(data);
				}
				
			}
		});
		return false;
	});
	
	// Edit Categories table
	$("body").delegate(".btn-edit-brand","click",function(){
		event.preventDefault();
		
		var brand_id = $(this).attr('bid');
		
		$.ajax({
			url : "experiment.php",
			method : "POST",
			data : {Brand_Edit:1,brand_id:brand_id},
			success : function(data){
				$("#show-brand-edit").html(data);
			}
		})
	});
	
	$("body").delegate(".btn-delete-brand","click",function(){
		event.preventDefault();
		
		var brand_id = $(this).attr('bid');
		
		$.ajax({
			url : "experiment.php",
			method : "POST",
			data : {Brand_Delete:1,brand_id:brand_id},
			success : function(data){
				if(confirm("Are you sure to restore this data?")){
					Brand_table();
					alert(data);
				}
			}
		});
		
	});
	
	// for brand close
 	$("body").delegate(".btn-close-brand","click",function(){
		event.preventDefault();
		$("#show-brand-edit").hide();
	});
	
	// for category edit
	$("body").delegate(".btn-edit-brand","click",function(){
		event.preventDefault();
		$("#show-brand-edit").show();
	});
	
	
	
	
	// SHOW Archive for Categories!!! Note: this files is not deleted!
	showCategoryArchive();
	function showCategoryArchive(){
		$.ajax({
			url : "experiment.php",
			method : "POST",
			data : {show_Archive_Category:1},
			success : function(data){
				$("#show-archive-category").html(data);
		}
		});
	}
	
	// for Restore Category Buttons!
	$("body").delegate(".btn-archive-category","click",function(){
		event.preventDefault();
		var archive_id = $(this).attr('archive_cat_id');
		$.ajax({
			url : "experiment.php",
			method : "POST",
			data : {Restore_Category:1,archive_id:archive_id},
			success : function(data){
				if(confirm("Are you sure to restore this data?")){
					showCategoryArchive();
					$("#show-archive-category").html(data);
					if(data){
						alert("Restore!	");
					}
					
				}
			}
		});
		
	});
	
	
	
	// SHOW Archive for Brand!!! Note: this files is not deleted!
	showBrandArchive();
	function showBrandArchive(){
		$.ajax({
			url : "experiment.php",
			method : "POST",
			data : {show_Archive_Brand:1},
			success : function(data){
				$("#show-archive-brand").html(data);
		}
		});
	}
	
	$("body").delegate(".btn-archive-brand","click",function(){
		event.preventDefault();
		var brand_id = $(this).attr('archive_brand_id');
		$.ajax({
			url : "experiment.php",
			method : "POST",
			data : {Restore_Brand:1,brand_id:brand_id},
			success : function(data){
				if(confirm("Are you sure to restore this data?")){
					showBrandArchive();
					$("#show-archive-brand").html(data);
					if(data){
						alert("Restore!");
					}
				}
			}
		});
		
	});
	
	//FOr Account Management Table
	showAccountManagement();
	function showAccountManagement(){
		$.ajax({
			url : "experiment.php",
			method : "POST",
			data : {show_Account_Management:1},
			success : function(data){
				$("#show_account_management").html(data);
		}
		});
	}
	
	// FOR account Management Delete button
	$("body").delegate(".btn-delete-accounts","click",function(){
		event.preventDefault();
		var uid = $(this).attr('uid');
		$.ajax({
			url : "experiment.php",
			method : "POST",
			data : {Delete_Accounts_Management:1,account_id:uid},
			success : function(data){
				if(confirm("Are you sure to delete this data?")){
					showAccountManagement();
					if(data){
						alert("Delete Success");
					}
				}
				
			}
		});
		
	});
	
	showAccountArchive();
	function showAccountArchive(){
		$.ajax({
			url : "experiment.php",
			method : "POST",
			data : {show_Account_Archive:1},
			success : function(data){
				$("#show_account_archive").html(data);
				
		}
		});
	}
	
	$("body").delegate(".btn-archive-accounts","click",function(){
		event.preventDefault();
		var update_uid = $(this).attr('update_uid');
		$.ajax({
			url : "experiment.php",
			method : "POST",
			data : {Restore_Accounts_Archive:1,account_id:update_uid},
			success : function(data){
				if(confirm("Are you sure to restore this data?")){
					showAccountArchive();
					if(data){
						alert("Restore Success");
					}
				}
				
			}
		});
		
	});
	
	// page number for USERS ACCOUNT
	pageno_account();
	function pageno_account(){
		$.ajax({
			url : "experiment.php",
			method : "POST",
			data : {pageno_account:1},
			success :function(data){
				$("#pageno_account").html(data);
		}
		});
	}
	
	
	// for sales table
	Order_Table();
	function Order_Table(){
		$.ajax({
			url : "experiment.php",
			method : "POST",
			data : {Order_Table:1},
			success : function(data){
				$("#show_Sales_Table").html(data);
			}
		});
	}
	
	// For Pagination of Product Maintenance
	pageOrders();
	function pageOrders(){
		$.ajax({
			url : "experiment.php",
			method : "POST",
			data : {page_Orders_Table:1},
			success :function(data){
				$("#pagenum_orders_table").html(data);
				
		}
		})
	}
	
	
//	for Pagintion of Product Maintenance
	$("body").delegate("#page_sales","click",function(){
		var pn = $(this).attr("page_sales");
		$.ajax({
			url : "experiment.php",
			method : "POST",
			data : {Order_Table:1,set_Page:1,page_Number:pn},
			success :function(data){
				$("#show_Sales_Table").html(data);
				
		}
		});
	});
	
	// Customer's Order
	Order_Table();
	function Order_Table(){
		$.ajax({
			url : "experiment.php",
			method : "POST",
			data : {Order_Table:1},
			success : function(data){
				$("#show_Sales_Table").html(data);
			}
		});
	}
	
	// FOR date search!!
	$("body").delegate("#btn-date-search","click",function(){
		
	 	var d1 = $("#datepicker1").val();
		
		
		$.ajax({
			url : "experiment.php",
			method : "POST",
			data : {Date_Result:1,Date1:d1},
			success : function(data){
				$("#show_Sales_Table").html(data);
			}
		});
		
	});
		
	
	
	
	//SHOW INVENTORY TABLE
	Inventory_Table();
	function Inventory_Table(){
		$.ajax({
			url : "experiment.php",
			method : "POST",
			data : {Inventory_Table:1},
			success : function(data){
				$("#show_inventory").html(data);
			}
		});
	}
	
	
	// SHOW PAGINATION
	pageInventory();
	function pageInventory(){
		$.ajax({
			url : "experiment.php",
			method : "POST",
			data : {page_Inventory_Table:1},
			success :function(data){
				$("#pagenum_inventory_table").html(data);
				
		}
		})
	}
	
	
	
	$("body").delegate("#page_invt","click",function(){
		var pn = $(this).attr("page_invt");
		$.ajax({
			url : "experiment.php",
			method : "POST",
			data : {Inventory_Table:1,set_Page:1,page_Number:pn},
			success :function(data){
				$("#show_inventory").html(data);
				
		}
		});
	});
	
	showTable();
	function showTable(){
		$.ajax({
			url : "experiment.php",
			method : "POST",
			data : {showTable:1},
			success :function(data){
				$("#show_table").html(data);
				
		}
		})
	}
	
	// For add stocks
	$("body").delegate("#btn_add_stocks","click",function(){
		event.preventDefault();
		var tid = $(this).attr('passid');
		var add = $("#add_stocks").val();
		var name = $(this).attr('admin');
		$.ajax({
			url : "experiment.php",
			method : "POST",
			data : {Add_Stocks_Inventory:1,pid:tid,addstock:add,admin_name:name},
			success : function(data){
//				$("#show_inventory_add").html(data);
				if(data){
					alert(data);
					location.reload();
				}

			}
		});
		
     });
	
	$("body").delegate("#show_add_stocks","click",function(){
			var pid = $(this).attr('pid');// get the value of pid
//			var name = $(this).attr('admin_name');
			$.ajax({
			url : "experiment.php",
			method : "POST",
			data : {Show_Invetory_Stocks:1,pid:pid},
			success : function(data){
				$("#show_inventory_add").html(data);
				
			}
		});
		
	});
	
	$("body").delegate("#close_add_inventory","click",function(){
		
		$("#show_inventory_add").hide();
	});
	
	
	//For delete Order
	$("body").delegate("#delivered_items","click",function(){
		event.preventDefault();
//		var pid = $("#fname").val();
		var tid = $(this).attr('trans');
		var fname = $(this).attr('fname');
		var lname = $(this).attr('lname');
		var prod_title = $(this).attr('prod_title');
		var qty = $(this).attr('qty');
		var price = $(this).attr('price');
		var total = $(this).attr('total');
		var billing = $(this).attr('billing');
		
		$.ajax({
			url : "experiment.php",
			method : "POST",
			data : {Delivered:1,del_id:tid,fname:fname,lname:lname,prod_title:prod_title,qty:qty,price:price,total:total,billing:billing},
			success :function(data){
				if(confirm("Are you sure to deliver")){
					if(data){
					location.reload();
				}
				}
				
		}
		});
	});
	
	
	// Admin Promotion
	$("body").delegate("#promote_user","click",function(){
		var uid = $(this).attr('user_id');
	
		$.ajax({
			url : "experiment.php",
			method : "POST",
			data : {Promote_user:1,uid:uid},
			success : function(data){
				if(confirm("Are you sure to promote")){
					location.reload();
				}
			}
		});
	});
	
	// Admin Demote
	$("body").delegate("#demote_user","click",function(){
		var uid = $(this).attr('user_id');
	
		$.ajax({
			url : "experiment.php",
			method : "POST",
			data : {demote_user:1,uid:uid},
			success : function(data){
				if(confirm("Are you sure to demote")){
					location.reload();
				}
				
			}
		});
	});
	
	// Add Register Employee
	$("body").delegate("#add_employee","click",function(){
//		var uid = $(this).attr('user_id');
		var uname = $("#username").val();
		var pass = $("#password").val();
		var employ = $("#emp_name").val();
	
		$.ajax({
			url : "experiment.php",
			method : "POST",
			data : {Add_Employee:1,uname:uname,pass:pass,employee:employ},
			success : function(data){
				if(data){
					alert(data);
				}
				
			}
		});
		
		
	});
	
	
	
	// FOr Modal
		  $("body").delegate("#myBtn","click",function(){
			  event.preventDefault();
                $("#myModal").modal();
			  
                });
	
	// FOR Date Search
	 $("body").delegate("#datecollect","click",function(){
		var date1 = $("#datepicker1").val();
		var date2 = $("#datepicker2").val();
		
		alert("search dates");
	});
	
	// For Show ALl
	 $("body").delegate("#showAll","click",function(){
		alert("Show All Records");
	});
	
	
	 $("body").delegate("#outofstocks","click",function(){
		$("#outofstock").submit();
	});
	
	$("body").delegate("#critical","click",function(){
		$("#critical").submit();
	});
	
	$("body").delegate("#show_All","click",function(){
		$("#show_all").submit();
	});
	
	$("body").delegate("#safe_level","click",function(){
		$("#safelevel").submit();
	});
	

	$("body").delegate("#btn_login","click",function(){
		$("#dash_login").submit();
	});
	
	
//	
	$("body").delegate("#remove_user","click",function(){
		
		var uid = $(this).attr('rev_id');
//		alert(uid);
		$.ajax({
			url : "experiment.php",
			method : "POST",
			data : {Remove_Employee:1,user_id:uid},
			success : function(data){
				if(confirm("Are you sure to delete")){
					alert(data);
					location.reload();
				}
				
			}
		});
	});
	
	$("body").delegate("#update_user","click",function(){
		var uid = $(this).attr('up_id');
		
		$.ajax({
			url : "experiment.php",
			method : "POST",
			data : {Recover_Employee:1,user_id:uid},
			success : function(data){
				if(confirm("Are you sure to delete")){
					alert(data);
					location.reload();
				}
			
			}
		});
		
		
	});
	
	$("body").delegate("#web_save","click",function(){
		$("#web_content").submit();
		
	});
	
	
	$("#notify_click").click(function(event){
		event.preventDefault();
		$.ajax({
			url : "experiment.php",
			method : "POST",
			data : {notification:1},
			success : function(data){
			 $("#notifcation").html(data);	
		}
		})
	})
	
	cart_count();
	function cart_count(){
		$.ajax({
			url : "experiment.php",
			method : "POST",
			data : {cart_count:1},
			success : function(data){
				$(".badge").html(data);
		}
		})
	}
});