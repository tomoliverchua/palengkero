<?php include"header.php"; ?>

<div id="content-wrapper">

        <div class="container">

          <!-- Icon Cards-->
          <div class="row">

            <div class="col-xl-3 col-sm-6 mb-3">
              <div class="card text-white bg-warning o-hidden h-100">
                <div class="card-body">
                  <div class="mr-5" id="buyer-count">
                    <span class="fas fa-spin fa-circle-notch"></span>
                  </div>
                </div>
              </div>
            </div>

            <div class="col-xl-3 col-sm-6 mb-3">
              <div class="card text-white bg-danger o-hidden h-100">
                <div class="card-body">
                  <div class="mr-5" id="seller-count">
                    <span class="fas fa-spin fa-circle-notch"></span>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-xl-3 col-sm-6 mb-3">
              <div class="card text-white bg-success o-hidden h-100">
                <div class="card-body">
                  <div class="mr-5" id="total-sales">
                    <span class="fas fa-spin fa-circle-notch"></span>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col">
              <h3>Items by area</h3>
              <div class="row" id="area-items">
                <div class="col">
                  <p class="text-center"><span class="fas fa-spin fa-circle-notch"></span></p>
                </div>
              </div>
            </div>
          </div>
         
        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- /.content-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fas fa-angle-up"></i>
    </a>

    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin.min.js"></script>

    <script>

      fetch("/api/v1/users")
        .then(res => res.json())
        .then(json => {
          if (json.error) {
            $('#buyer-count').text("Couldn't get buyer count");
            $('#seller-count').text("Couldn't get seller count");
          } else {
            const buyers = json.details.filter(user => user.type == 'buyer');
            const sellers = json.details.filter(user => user.type == 'seller');
            $('#buyer-count').text(`${buyers.length} buyer${buyers.length != 1 ? 's' : ''}`);
            $('#seller-count').text(`${sellers.length} seller${sellers.length != 1 ? 's' : ''}`);
          }
        });
      
      fetch("/api/v1/total-sales")
        .then(res => res.json())
        .then(json => {
          if (json.error) {
            $('#total-sales').text("Couldn't get total sales");
          } else {
            $('#total-sales').text(`Total sales: PHP ${json.details.total_sales}`);
          }
        });

      fetch("/api/v1/items/by-area")
        .then(res => res.json())
        .then(json => {
          $('#area-items').empty();

          if (json.error) {
            $('#area-items').html("<p>Couldn't get item data</p>");
          } else {
            const items = json.details;
            for (const area in items) {
              const col = $('<div class="col-12">');
              col.append(`<h4>${area}</h4>`);
              
              const itemRow = $('<div class="row">');

              for (const item of items[area]) {
                itemRow.append(`<div class="col-sm-4">
                  <div class="card bg-light mb-2">
                    <div class="card-body">
                      <h5 class="card-title">${item.name}</h5>
                      <p class="card-subtitle text-muted">${item.seller}</p>
                      <p class="card-text text-muted">${item.quantity} units (PHP ${item.price}/unit)</p>
                    </div>
                  </div>
                </div>`);
              }

              col.append(itemRow);
              $('#area-items').append(col);
            }
          }
        });

    </script>
  </body>

</html>
