<?php

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

require 'vendor/autoload.php';
require 'database.php';

$c = new \Slim\Container([
    'settings' => ['displayErrorDetails' => true]
]);
$c->db = new PdoWrapper;
$c['view'] = new \Slim\Views\PhpRenderer('templates');


$app = new \Slim\App($c);

$app->add(function ($req, $res, $next) {
    $response = $next($req, $res);
    return $response
        ->withHeader('Access-Control-Allow-Origin', '*')
        ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
        ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
});

$app->group('/v1', function (\Slim\App $app) {

    /*
        fetch all items
     */
    $app->get('/items', function (Request $request, Response $response, array $args) {
        $params = $request->getQueryParams();

        $where = "";
        $sql_args = [];

        if (array_key_exists('area', $params)) {
            $where .= "a.name = ? AND ";
            $sql_args[] = mb_strtolower($params['area']);
        }
        if (array_key_exists('category', $params)) {
            $where .= "c.name = ? AND ";
            $sql_args[] = mb_strtolower($params['category']);
        }

        $where .= "1";

        try {
            $items = $this->db
                ->run("SELECT items.id, g.name, s.name AS seller, a.name AS area, quantity, price, c.name AS category
                        FROM items
                        INNER JOIN users s ON s.id = seller_id
                        INNER JOIN goods g ON g.id = good_id
                        INNER JOIN areas a ON a.id = items.area_id
                        INNER JOIN categories c ON c.id = category_id
                        WHERE $where", $sql_args)
                ->fetchAll();
        } catch (\Exception $e) {
            return $response->withJson($e);
        }

        if ($items) {
            return $response->withJson(["error" => false, "details" => $items]);
        } else {
            return $response->withStatus(404)->withJson(["error" => true, "message" => "No items found."]);
        }
    });

    /*
        fetch all items grouped by area.
    */
    $app->get('/items/by-area', function (Request $req, Response $res, array $args) {
        $items = $this->db
            ->run("SELECT items.id, g.name, s.name AS seller, a.name AS area, quantity, price, c.name AS category
                        FROM items
                        INNER JOIN users s ON s.id = seller_id
                        INNER JOIN goods g ON g.id = good_id
                        INNER JOIN areas a ON a.id = items.area_id
                        INNER JOIN categories c ON c.id = category_id
                        ORDER BY area")
            ->fetchAll();
        if ($items) {
            $itemsByArea = [];
            $area = '';
            foreach ($items as $item) {
                if ($item['area'] != $area) {
                    $area = $item['area'];
                    $itemsByArea[$area] = [];
                }
                $itemsByArea[$area][] = $item;
            }
            return $res->withJson(["error" => false, "details" => $itemsByArea]);
        } else {
            return $res->withJson(["error" => true, "message" => "No items found."], 404);
        }
    });

    /*
        items ng particular seller

        - seller_id - id ng seller sa users table
    */
    $app->get('/items/{seller_id}', function (Request $request, Response $response, array $args) {
        try {
            $items = $this->db
                ->run("SELECT items.id, g.name, s.name AS seller, a.name AS area, quantity, price, c.name AS category
                        FROM items
                        INNER JOIN users s ON s.id = seller_id
                        INNER JOIN goods g ON g.id = good_id
                        INNER JOIN areas a ON a.id = items.area_id
                        INNER JOIN categories c ON c.id = category_id
                        WHERE seller_id = ?", [$args['seller_id']])
                ->fetchAll();
        } catch (\Exception $e) {
            return $response->withJson($e);
        }

        if ($items) {
            return $response->withJson(["error" => false, "details" => $items]);
        } else {
            return $response->withStatus(404)->withJson(["error" => true, "message" => "No items found."]);
        }
    });


    /*
        create item.

        body params:
        - good_id - id ng particular good sa goods table
        - area_id - id ng particular area sa areas table
        - quantity - quantity for sale
        - price - price per unit ng item
        - seller_id - id ng seller sa users id
     */
    $app->post('/items', function (Request $req, Response $res, array $args) {
        $body = $req->getParsedBody();
        $success = $this->db
            ->run(
                'INSERT INTO items (good_id, area_id, quantity, price, seller_id) VALUES (?, ?, ?, ?, ?)',
                [
                    $body['good_id'],
                    $body['area_id'],
                    $body['quantity'],
                    $body['price'],
                    $body['seller_id'],
                ]
            );

        if ($success) {
            $insertId = $this->db->lastInsertId();
            $newItem = $this->db
                ->run("SELECT items.id, g.name, s.name AS seller, a.name AS area, quantity, price, c.name AS category
                        FROM items
                        INNER JOIN users s ON s.id = seller_id
                        INNER JOIN goods g ON g.id = good_id
                        INNER JOIN areas a ON a.id = items.area_id
                        INNER JOIN categories c ON c.id = category_id
                        WHERE items.id = ?", [$insertId])
                ->fetch();
            return $res->withJson(
                ['error' => false, 'message' => 'Item added.', 'details' => $newItem],
                201
            );
        } else {
            return $res->withJson(
                ['error' => true, 'message' => "Couldn't add item."],
                422
            );
        }
    });

    /*
        increase/decrease item quantity

        - id - id ng item sa items table

        body params:
        - quantity - kung ilan idadagdag/ibabawas (negative para bumawas)
     */
    $app->post('/items/update/{id}', function (Request $req, Response $res, array $args) {
        $body = $req->getParsedBody();
        $success = $this->db
            ->run(
                'UPDATE items SET quantity = quantity + ? WHERE id = ?',
                [$body['quantity'], $args['id']]
            );
        if ($success) {
            return $res->withJson(['error' => false, 'message' => 'Item updated.']);
        } else {
            return $res->withJson(['error' => true, 'message' => "Couldn't update item."]);
        }
    });

    /*
        delete particular item
        - id - id ng item to delete sa items table
     */
    $app->post('/items/delete/{id}', function (Request $req, Response $res, array $args) {
        $this->db->run('DELETE FROM items WHERE id = ?', [$args['id']]);
        return $res->withJson(['error' => false, 'message' => 'Item deleted.']);
    });

    // NOTE TO SELF. Buyer id should be passed using a more suitable channel (e.g., tokens)
    /*
        transaction headers ng particular buyer. Dito manggagaling yung id para makapag-fetch
        ng buyer transaction details.

        - buyer_id - id ng buyer sa users table
     */
    $app->get('/transaction-headers/{buyer_id}', function (Request $request, Response $response, array $args) {
        $transactions = $this->db
            ->run(
                'SELECT transactions.id, `date`, a.name AS area,
                (select sum(d.quantity * items.price) from transaction_details d inner join items on items.id = d.item_id where transactions.id = d.transaction_id) as total
                FROM transactions
                INNER JOIN areas a ON a.id = area_id
                WHERE buyer_id = ? ORDER BY `date` DESC',
                [$args['buyer_id']]
            )
            ->fetchAll();

        if ($transactions) {
            return $response->withJson(["error" => false, "details" => $transactions]);
        } else {
            return $response->withStatus(404)->withJson(["error" => true, "message" => "No transactions found."]);
        }
    });

    /*
        transaction details ng given transaction
        - transaction_id - id ng transaction header sa transactions table
     */
    $app->get('/transaction-details/{transaction_id}', function (Request $request, Response $response, array $args) {
        $details = $this->db
            ->run(
                'SELECT g.name as item, d.quantity, i.price AS price_per_unit, (SELECT d.quantity * price_per_unit) AS total_price, s.name AS seller, a.name AS area
                FROM transaction_details d
                INNER JOIN items i ON i.id = item_id
                INNER JOIN goods g ON g.id = good_id
                INNER JOIN users s ON s.id = seller_id
                INNER JOIN areas a ON a.id = i.area_id
                WHERE transaction_id = ?',
                [$args['transaction_id']]
            )
            ->fetchAll();

        if ($details) {
            return $response->withJson(["error" => false, "details" => $details]);
        } else {
            return $response->withStatus(404)->withJson(["error" => true, "message" => "No transaction details found."]);
        }
    });

    /*
        transaction history ng seller
        - seller_id - id ng seller sa users table
     */
    $app->get('/seller-transactions/{seller_id}', function (Request $req, Response $res, array $args) {
        $details = $this->db
            ->run(
                'SELECT
                    g.name as item,
                    d.quantity,
                    i.price as price_per_unit,
                    (SELECT d.quantity * price_per_unit) as total_price,
                    a.name as area,
                    (SELECT `date` FROM transactions WHERE transactions.id = d.transaction_id) AS `date`
                FROM transaction_details d
                INNER JOIN items i on i.id = item_id
                INNER JOIN goods g on g.id = good_id
                INNER JOIN areas a on a.id = i.area_id
                WHERE seller_id = ?
                ORDER BY `date` DESC',
                [$args['seller_id']]
            )
            ->fetchAll();

        if ($details) {
            return $res->withJson(["error" => false, "details" => $details]);
        } else {
            return $res->withStatus(404)->withJson(["error" => true, "message" => "No transaction details found."]);
        }
    });

    /*
        create transaction detail for item. assuming na meron nang transaction
        header para sa buong transaction.
        
        body params:
        - item_id - id ng item na isasama sa transaction sa items table
        - quantity - quantity to buy
        - transaction_id - id ng transaction header sa transactions table
     */
    $app->post('/transact/item', function (Request $request, Response $response, array $args) {
        $body = $request->getParsedBody();
        $item_id = $body['item_id'];
        $quantity = (int) $body['quantity'];

        $item = $this->db
            ->run('SELECT quantity FROM items WHERE id = ?', [$item_id])
            ->fetch();

        if ($quantity > $item['quantity']) {
            return $response->withStatus(400)->withJson([
                "error" => true,
                "message" => "Can't exceed quantity.",
            ]);
        }

        $this->db
            ->run('UPDATE items SET quantity = ? WHERE id = ?', [$item['quantity'] - $quantity, $item_id]);

        $success = $this->db
            ->run(
                'INSERT INTO transaction_details (transaction_id, item_id, quantity) VALUES (?, ?, ?)',
                [
                    $body['transaction_id'],
                    $body['item_id'],
                    $body['quantity'],
                ]
            );

        $json = ["error" => !$success];
        if ($success) {
            return $response->withStatus(201)->withJson($json);
        } else {
            return $response->withStatus(422)->withJson($json);
        }
    });

    /*
        create transaction header. required para gumawa ng transaction details.

        body params
        - buyer_id - id ng buyer sa users table
        - area_id - id ng area kung saan binili yung items sa areas table
     */
    $app->post('/transact/header', function (Request $request, Response $response, array $args) {
        $body = $request->getParsedBody();
        $success = $this->db
            ->run(
                'INSERT INTO transactions (`date`, buyer_id, area_id) VALUES (CURRENT_TIMESTAMP, ?, ?)',
                [
                    $body['buyer_id'],
                    $body['area_id'],
                ]
            );

        $json = ["error" => !$success];
        if ($success) {
            return $response->withStatus(201)->withJson($json);
        } else {
            return $response->withStatus(422)->withJson($json);
        }
    });

    /*
        creates url for paypal form. 

        - refer to paypal form params sa messenger/slack/etc.
     */
    $app->post('/pay', function (Request $req, Response $res, array $args) {
        $body = $req->getParsedBody();

        $params = [];
        foreach ($body as $k => $v) {
            $params[] = "$k=$v";
        }
        // Probably best to url-encode this string
        $p = implode("&", $params);

        return $res->withJson([
            "error" => false,
            "details" => [
                "url" => "http://192.168.88.87/api/v1/paypal-form?$p"
            ]
        ], 200, JSON_UNESCAPED_SLASHES);
    });

    /* 
        endpoint para sa paypal form. provided ng /pay endpoint yung query params.
     */
    $app->get('/paypal-form', function (Request $req, Response $res, array $args) {
        $params = $req->getQueryParams();
        $res = $this->view->render($res, 'paypal.phtml', ['paypal' => $params]);
        return $res;
    });

    /*
        get user details
        - id - id ng user sa users table
     */
    $app->get('/user/{id}', function (Request $req, Response $res, array $args) {
        $users = $this->db
            ->run(
                'SELECT u.name AS user, a.name AS area
                FROM users u
                INNER JOIN areas a ON area_id = a.id
                WHERE u.id = ?',
                [$args['id']]
            )
            ->fetchAll();
        if ($users) {
            return $res->withJson(['error' => false, 'details' => $users,]);
        } else {
            return $res->withJson(
                ['error' => true, 'message' => 'User not found'],
                404
            );
        }
    });

    $app->get('/users', function (Request $req, Response $res, array $args) {
        $users = $this->db->run('SELECT * FROM users')->fetchAll();
        if ($users) {
            return $res->withJson(["error" => false, "details" => $users]);
        } else {
            return $res->withJson(["error" => true, "message" => "No users found"]);
        }
    });

    $app->get('/buyers', function (Request $req, Response $res, array $args) {
        $buyers = $this->db->run('SELECT * FROM users WHERE type = "buyer"')->fetchAll();
        if ($buyers) {
            return $res->withJson(["error" => false, "details" => $buyers]);
        } else {
            return $res->withJson(["error" => true, "message" => "No buyers found"]);
        }
    });

    $app->get('/total-sales', function (Request $req, Response $res, array $args) {
        $sum = $this->db
            ->run('SELECT SUM(d.quantity * price) AS total_sales
                    FROM transaction_details d
                    inner join items on items.id = item_id')
            ->fetch();
        if ($sum !== null) {
            return $res->withJson(['error' => false, 'details' => $sum]);
        } else {
            return $res->withJson(['error' => true, 'message' => "Couldn't fetch sum"]);
        }
    });

    /*
        fetch average prices in all areas for given good. if an area is specified,
        only fetches the average price for that area.
     */
    $app->get('/average-prices[/{good_id}[/{area_id}]]', function (Request $req, Response $res, array $args) {
        $where = '1';
        $sqlArgs = [];

        if (array_key_exists('good_id', $args)) {
            $where .= ' and good_id = ?';
            $sqlArgs[] = $args['good_id'];
        }

        if (array_key_exists('area_id', $args)) {
            $where .= ' and area_id = ?';
            $sqlArgs[] = $args['area_id'];
        }

        $prices = $this->db
            ->run("SELECT g.name as good, a.name as area, average_price
                    FROM average_prices ap
                    INNER JOIN goods g ON good_id = g.id
                    INNER JOIN areas a ON area_id = a.id
                    WHERE $where", $sqlArgs)
            ->fetchAll();
        if ($prices) {
            return $res->withJson(['error' => false, 'details' => $prices]);
        } else {
            return $res->withJson(['error' => true, 'message' => 'No average prices found'], 404);
        }
    });
});

$app->run();
