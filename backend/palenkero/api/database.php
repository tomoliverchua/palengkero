<?php

class PdoWrapper extends \PDO {
    private const DB_HOST = 'localhost';
    private const DB_NAME = 'palenkero';
    private const DB_PORT = '';
    private const DB_CHARSET = 'utf8mb4';

    private const DB_USER = 'root';
    private const DB_PASSWORD = '';

    public function __construct()
    {
        $pdo_dsn = 'mysql:host=' . self::DB_HOST . ';dbname=' . self::DB_NAME . ';port=' . self::DB_PORT . ';charset=' . self::DB_CHARSET;
        $pdo_options = [
            \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION,
            \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC,
            \PDO::ATTR_EMULATE_PREPARES => false,
        ];
        parent::__construct($pdo_dsn, self::DB_USER, self::DB_PASSWORD, $pdo_options);
    }

    public function run(string $sql, ?array $args = null)
    {
        if (!$args) {
            return $this->query($sql);
        }

        $stmt = $this->prepare($sql);
        $stmt->execute($args);
        return $stmt;
    }
}